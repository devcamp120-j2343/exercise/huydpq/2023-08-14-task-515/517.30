import { Component } from "react";

class BodyInput extends Component {
    change = (event) => {
        console.log(event.target.value)
        this.props.updateInputMessaeProp(event.target.value)
    }

    click = () => {
        console.log('Gửi thông điệp được ấn')
        this.props.updateOutputMessageProp()
    }
    render() {
        // let {inputMessageProp} = this.props
        return (
           <>
            <div className="row mt-4">
                <label>Message cho bạn 12 tháng tới</label>
                <input placeholder="Nhập message của bạn vào đây" className="form-control mt-3" onChange={this.change} value={this.props.inputMessageProp}></input>
            </div>
            <div className="row mt-3">
            <button className="btn btn-success m-auto" style={{ width: "150px" }} onClick={this.click}>Gửi thông điệp</button>
        </div>
           </>
        )
    }
}

export default BodyInput