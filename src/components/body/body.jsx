import { Component } from "react";
import BodyInput from "./bodyInput/bodyInput";
import BodyOutput from "./bodyOutput/bodyOutout";

class Body extends Component {
    constructor(props){
        super(props)
        this.state = {
            inputMessage: "",
            outputMessage: [],
            likeDisplay: false
        }
    }

    updateInputMessage = (message) => {
        this.setState ({
            inputMessage: message
        })
    }

    updateOutputMessage = () => {
        if(this.state.inputMessage) {
            this.setState({
                outputMessage: [...this.state.outputMessage, this.state.inputMessage],
                likeDisplay: true
            })
        }
    }

    render() {
        return (
            <>
                <BodyInput 
                inputMessageProp = {this.state.inputMessage} 
                updateInputMessaeProp = {this.updateInputMessage}
                updateOutputMessageProp={this.updateOutputMessage}
                />
                <BodyOutput outputMessageProp = {this.state.outputMessage} likeDisplayProp = {this.state.likeDisplay}/>
            </>
        )
    }
}

export default Body