import { Component } from "react";
import HeaderText from "./headerText/headerText";
import HeaderImage from "./headerImage/headerImage";

class Header extends Component {

    render() {
        return (
            <>
               <HeaderText/>
                <HeaderImage/>
            </>
        )
    }
}

export default Header;
