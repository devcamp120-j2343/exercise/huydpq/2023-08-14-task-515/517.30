import "bootstrap/dist/css/bootstrap.min.css"

import Header from "./components/header/header";
import Body from "./components/body/body";
function App() {
  return (
    <div className="container text-center">
      <Header/>
      <Body/>
    </div>
  );
}

export default App;
